package objects;

import javafx.scene.canvas.GraphicsContext;
import objects.tools.*;

import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class SpaceCraft implements Movable, Renderable{

    private int x;
    private int y;
    private final int size;
    private int lives;
    private Image spaceCraftBody;

    SpaceCraft(int x, int y, String path, int size, int lives) throws FileNotFoundException {
        this.x = x;
        this.y = y;
        this.size = size;
        this.lives = lives;
        FileInputStream is = new FileInputStream(path);
        this.spaceCraftBody = new Image(is, size, size, true,true);

    }

    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }
    public int getSize(){return this.size;}
    public int getLivesNumber(){return this.lives;}
    private Image getSpaceCraftBody(){
        return this.spaceCraftBody;
    }

    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }
    public void setLives(int life){
        this.lives = life;
    }
    public void moveHorizontally(int step){ //positive right
        setPosition(this.getX() + step, this.getY());
    }
    public void moveVertically(int step){ //positive down
        setPosition(this.getX(), this.getY() + step);
    }

    @Override
    public void render(GraphicsContext gc) {
        gc.drawImage(this.getSpaceCraftBody(),this.getX(),this.getY());
    }

}
