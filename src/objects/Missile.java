package objects;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import objects.tools.Renderable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Missile implements Renderable {

    private int x;
    private int y;
    private Image missileBody;
    private final int power;

    public Missile(int x, int y, int size, int power) throws FileNotFoundException {

        this.x = x;
        this.y = y;
        this.power = power;
        FileInputStream is = new FileInputStream("resources/images/laserShooted.png");
        this.missileBody = new Image(is, size, size,true,false);

    }

    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }

    public int getPower(){return this.power;}
    private Image getMissileBody(){
        return this.missileBody;
    }

    private void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void moveVertically(int step) {
        setPosition(this.getX(), this.getY()+step);
    }

    @Override
    public void render(GraphicsContext gc) {
        gc.drawImage(this.getMissileBody(), this.getX(), this.getY());
    }
}
