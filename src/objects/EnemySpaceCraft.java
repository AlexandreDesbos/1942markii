package objects;

import java.io.FileNotFoundException;

public class EnemySpaceCraft extends SpaceCraft {

    private int pts;

//    public EnemySpaceCraft(int x, int y, int size, int lives) throws FileNotFoundException {
//        super(x, y, "src/images/spacecraftEnemy.png", size, lives);
//        this.pts = 100;
//    }
    public EnemySpaceCraft(int x, int y, int size) throws FileNotFoundException {
        super(x, y, "resources/images/spacecraftEnemy.png", size, 1);
        this.pts = 100;


    }


    public int getPts(){return this.pts;}
    public void setPts(int pts){
        this.pts = pts;
    }




}
