package objects.tools;

public interface Movable {
    void moveHorizontally(int step);
    void moveVertically(int step);

}
