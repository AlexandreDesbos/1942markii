package objects;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import objects.tools.Renderable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Background implements Renderable {

    private Image backgroundImg;

    public Background(double width, double height) throws FileNotFoundException {

        FileInputStream is = new FileInputStream("resources/images/space.png");
        this.backgroundImg = new Image(is,width,height,true,true);
    }

    @Override
    public void render(GraphicsContext gc) {
        gc.drawImage(this.backgroundImg,0,0);
    }
}
