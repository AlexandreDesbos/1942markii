package objects;

import java.io.FileNotFoundException;

public class HeroSpaceCraft extends SpaceCraft {

    private int score;

    public HeroSpaceCraft(int x, int y, int size, int lives) throws FileNotFoundException {
        super(x, y, "resources/images/spacecraftHero.png", size, lives );
        this.score = 0;
    }
    public int getScore(){ return this.score;}
    public void setScore(int addedScore){
        this.score = this.getScore() + addedScore;
    }


}
