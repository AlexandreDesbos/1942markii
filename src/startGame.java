import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;


import objects.*;
import objects.tools.Renderable;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;


public class startGame extends Application {

    @Override
    public void start(Stage theStage) throws Exception {


        theStage.setHeight(1000);
        theStage.setWidth(1000);

        Group root = new Group();
        Scene theScene = new Scene(root);
        theStage.setScene(theScene);

        Canvas canvas = new Canvas(1000, 1000);
        root.getChildren().add(canvas);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setFill( Color.WHITE);

//--------------------------------- variables ----------------------
        AnimationTimer game;
        final long startNanoTime;
        final int stepHero = 25;
        int speedEnemy = 1;


        Random random = new Random();

        List<Renderable> RenderableObjects = new CopyOnWriteArrayList<>();
        List<Missile> missileList = new CopyOnWriteArrayList<>();
        List<EnemySpaceCraft> enemyList = new CopyOnWriteArrayList<>();

        Background background = new Background(canvas.getWidth(),canvas.getHeight());
        HeroSpaceCraft spacecraftHero = new HeroSpaceCraft(450,(int)(canvas.getHeight()*0.8),80,3);


        FileInputStream explosionImage= new FileInputStream( "resources/images/explosion_10.png" );
        Image explosion = new Image(explosionImage);

        RenderableObjects.add(background);
        RenderableObjects.add(spacecraftHero);



        startNanoTime = System.nanoTime();

        game = new AnimationTimer()
        {
            double frequency = 4.0;
            double creationTime = frequency;
            int left_rightShoot =0;
            int pressed = 0;


            public void handle(long currentNanoTime){

                Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 25 );
                gc.setFont(theFont);
                double time = (currentNanoTime - startNanoTime)/1000000000.0;

//----------------------------------------- RENDERING OBJECTS --------------------------------------------------

                for (Renderable obj:RenderableObjects) {
                    obj.render(gc);
                }

                gc.fillText( "lives : "+ spacecraftHero.getLivesNumber(), 20, 50 );
                gc.fillText("Score : "+ spacecraftHero.getScore(), 20, 90);

//----------------------------------------- MANAGE PLANE/MISSILE INTERACTIONS ----------------------------------
                for(Missile mi:missileList){
                    mi.moveVertically(-15);
                    if(mi.getY() < 0){
                        RenderableObjects.remove(mi);
                        missileList.remove(mi);
                    }
                    for (EnemySpaceCraft e:enemyList){
                        if(mi.getY() < (e.getY()+e.getSize())
                                && mi.getY() > e.getY()
                                && mi.getX() > e.getX()
                                && mi.getX() < (e.getX()+e.getSize()))
                        {
                            RenderableObjects.remove(mi);
                            missileList.remove(mi);
                            e.setLives(e.getLivesNumber()-mi.getPower());

                            if (e.getLivesNumber() <= 0){

                                gc.drawImage(explosion, e.getX(), e.getY() );
                                spacecraftHero.setScore(e.getPts());
                                int chance = random.nextInt(100);
                                if (chance > 90){
                                    spacecraftHero.setLives(spacecraftHero.getLivesNumber()+1);
                                }
                                RenderableObjects.remove(e);
                                enemyList.remove(e);
                            }
                        }
                    }
                }


                for (EnemySpaceCraft e:enemyList){
                    e.moveVertically(speedEnemy);
                    if(e.getY() > canvas.getHeight()){
                        enemyList.remove(e);
                        RenderableObjects.remove(e);
                        spacecraftHero.setLives(spacecraftHero.getLivesNumber()-1);
                    }
                }
                if(spacecraftHero.getLivesNumber()<0){
                    gc.fillText("GAME OVER",canvas.getWidth()*0.4, canvas.getHeight()*0.5);
                    this.stop();
                }


                if(time > 10.0){
                    frequency = 2.0;
                }
                if(time > 60.0){
                    frequency = 1.6;
                }
                if(time > 360.0){
                    frequency = 1.3;
                }
                if(time > creationTime ){


                    try {
//                        EnemyPlane enemy = new EnemyPlane(random.nextInt((int)canvas.getWidth()),0, random.nextInt(100-30+1)+30);
                        EnemySpaceCraft enemy = new EnemySpaceCraft(random.nextInt((int)canvas.getWidth()-200),-100, random.nextInt(70) + 30);
                        if (enemy.getSize() > 60){
                            enemy.setPts(200);
                            enemy.setLives(2);
                        }
                        enemyList.add(enemy);
                        RenderableObjects.add(enemy);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    creationTime += frequency;
                }

//----------------------------- HANDLE BUTTONS --------------------------------------

                theScene.setOnKeyPressed((KeyEvent event) -> {
                    if (event.getCode() != null){
                        switch (event.getCode()){
                            case LEFT:
//                                System.out.println("left");
                                spacecraftHero.moveHorizontally(-stepHero);
                                if (spacecraftHero.getX() < 0){
                                    spacecraftHero.setPosition(0,spacecraftHero.getY());
                                }
                                break;

                            case RIGHT:
                                spacecraftHero.moveHorizontally(stepHero);
                                if (spacecraftHero.getX()>canvas.getWidth()-spacecraftHero.getSize()){
                                    spacecraftHero.setPosition((int)canvas.getWidth()-spacecraftHero.getSize(),spacecraftHero.getY());
                                }
                                break;

                            case DOWN:

                                spacecraftHero.moveVertically(stepHero);
                                if(spacecraftHero.getY() > canvas.getHeight()-spacecraftHero.getSize()){
                                    spacecraftHero.setPosition(spacecraftHero.getX(),(int)canvas.getHeight()-spacecraftHero.getSize());
                                }
                                break;

                            case UP:
                                spacecraftHero.moveVertically(-stepHero);
                                if(spacecraftHero.getY() < 0){
                                    spacecraftHero.setPosition(spacecraftHero.getX(),0);
                                }

                                break;

                            case SPACE:

                                    if(left_rightShoot == 0){
                                        Missile missile = null;
                                        try {
                                            missile = new Missile(
                                                    spacecraftHero.getX(),
                                                    spacecraftHero.getY(),
                                                    40, 1
                                            );
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        }

                                        left_rightShoot = 1;
                                        RenderableObjects.add(missile);
                                        missileList.add(missile);

                                    }else{

                                        Missile missile = null;
                                        try {
                                            missile = new Missile(
                                                    spacecraftHero.getX()+(spacecraftHero.getSize()-16),
                                                       spacecraftHero.getY(),
                                                    40, 1
                                            );
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        }

                                        left_rightShoot = 0;
                                        RenderableObjects.add(missile);
                                        missileList.add(missile);
                                    }

                                break;

                            case ESCAPE:

                                if(pressed == 0 ){
                                    this.stop();
                                    pressed = 1;

//                                    Rectangle pause = new Rectangle(0,0,(int)canvas.getWidth(),(int)canvas.getHeight());
//                                    pause.setFill(Color.rgb(0,0,255,1.0));
                                    GraphicsContext pause = canvas.getGraphicsContext2D();

                                    pause.setFill(Color.rgb(0,0,0,0.7));
                                    pause.fillRect(0,0,(int)canvas.getWidth(),(int)canvas.getHeight());
                                    pause.setFill(Color.WHITE);

                                    Font pauseFont = Font.font( "Times New Roman", FontWeight.BOLD, 60 );
                                    pause.setFont(pauseFont);
                                    pause.fillText("PAUSE", canvas.getWidth()*0.4,canvas.getHeight()*0.4);


                                }
                                else{
                                    this.start();
                                    pressed = 0;
                                }

                            default:
                                break;

                        }
                    }
                });
            }
        };

        game.start();

        theStage.setTitle("1942 Mark II");
        theStage.show();
        theStage.setResizable(false);

    }
    public static void main(String[] args) {
        launch(args);
    }
}
